import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateComponent } from './pages/template/template.component';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { HomeComponent } from './components/home/home.component';
import { RestaurantePerfilComponent } from './components/restaurante/restaurante-perfil/restaurante-perfil.component';
import { RestauranteMenuComponent } from './components/restaurante/restaurante-menu/restaurante-menu.component';
import { PaypalComponent } from './components/paypal/paypal.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { QrComponent } from './components/qr/qr.component';
import { RestauranteEditComponent } from './components/restaurante/restaurante-edit/restaurante-edit.component';



const routes: Routes = [
  { path:'template', component: TemplateComponent },
  { path:'reactivo', component: ReactiveComponent },
  { path:'inicio', component: HomeComponent },
  { path:'restaurante-perfil', component: RestaurantePerfilComponent,  canActivate: [ AuthGuard ] },
  { path:'restaurante-menu/:codigo', component: RestauranteMenuComponent, /*canActivate: [ AuthGuard ]*/ },
  { path:'restaurante-edit/:codigo', component: RestauranteEditComponent, canActivate: [ AuthGuard ] },
  { path:'paypal', component: PaypalComponent, canActivate: [ AuthGuard ] },
  { path:'login', component: LoginComponent },
  { path:'qr/:codigo', component: QrComponent, canActivate: [ AuthGuard ] },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
