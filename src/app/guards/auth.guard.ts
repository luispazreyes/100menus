import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private auth: AuthService,
               private router: Router) {}

  canActivate( next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  Observable<boolean>  {

      return this.auth.isLoggedIn         // {1}
      .pipe(
        take(1),                              // {2} 
        map((isLoggedIn: boolean) => {         // {3}
          if (!isLoggedIn){
            this.router.navigate(['/login']);  // {4}
            return false;
          }
          return true;
        })
      );

    // if ( this.auth.estaAutenticado() ) {
    //   return true;
    // } else {
    //   this.router.navigateByUrl('/login');
    //   return false;
    // }
 
  }

}
