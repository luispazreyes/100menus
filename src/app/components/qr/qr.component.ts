import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RestauranteModel } from 'src/app/models/restaurante.model';
import { ActivatedRoute } from '@angular/router';
import { RestauranteService } from '../../services/restaurante.service';

// import htmlToImage from 'html-to-image';

import html2canvas from 'html2canvas';



@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.css']
})
export class QrComponent implements OnInit {
  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;

  constructor( private route: ActivatedRoute, private restauranteS: RestauranteService) { }

  public menus = [];
  public logo;
  public type = '';
  public usuario;
  public restaurante: RestauranteModel
  public show = true;
  public direccion: string;
  ngOnInit(): void {
    this.restauranteS.getRestaurantesByCode(this.route.snapshot.paramMap.get("codigo"))
      .subscribe( resp => {
        this.restaurante = resp[0];  
        this.direccion = `100menus.com/restaurante-menu/${this.restaurante.codigoRestaurante}`;      
      });
   
  }

  downloadImage(){
    this.show = true;
    html2canvas(this.screen.nativeElement).then(canvas => {
      this.canvas.nativeElement.width = 1200;
      this.canvas.nativeElement.height = 1200;
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = '100menus.png';
      this.downloadLink.nativeElement.click();
      this.show = false;
    });
  }

  //  print(){

  //    var node = document.getElementById('my-node');
 
  //     htmlToImage.toPng(node)
  //       .then(function (dataUrl) {
  //         var img = new Image();
  //         img.src = dataUrl;
  //         document.body.appendChild(img);
  //       })
  //       .catch(function (error) {
  //         console.error('oops, something went wrong!', error);
  //       });
  //  }

}
