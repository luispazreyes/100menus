import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { UsuarioModel } from '../../models/usuario.model';
import { AuthService } from '../../services/auth.service';

import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();
  recordarme = false;
  isLoggedIn$: Observable<boolean>;  

  constructor( private auth: AuthService,
               private router: Router,
               private usuarioS: UsuarioService ) { }

  ngOnInit() {

    

    if ( localStorage.getItem('email') ) {
      this.usuario.correo = localStorage.getItem('email');
      this.recordarme = true;
    }

  }

  login( form: NgForm ) {

    if (  form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();


    this.auth.login( this.usuario ).then( res => {
        Swal.close();

        this.usuarioS.getUsuarioByCorreo(this.usuario.correo).subscribe( res =>{
          localStorage.setItem('usuarioDocId', res[0].id);
          this.router.navigateByUrl('/restaurante-perfil');
        });

        if ( this.recordarme ) {
          localStorage.setItem('email', this.usuario.correo);
        }
    }).catch(err => {
      console.log(err.message);
        Swal.fire({
          icon: 'error',
          title: 'Error al autenticar',
          text: err.message
        });
    });
      // .subscribe( resp => {

      //   console.log(resp);
      //   Swal.close();

      //   if ( this.recordarme ) {
      //     localStorage.setItem('email', this.usuario.correo);
      //   }


      //   this.router.navigateByUrl('/inicio');

      // }, (err) => {

      //   console.log(err.error.error.message);
      //   Swal.fire({
      //     icon: 'error',
      //     title: 'Error al autenticar',
      //     text: err.error.error.message
      //   });
      // });

  }

}
