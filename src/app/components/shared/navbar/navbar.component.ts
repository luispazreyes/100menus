import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  auth: boolean;
  isLoggedIn$: Observable<boolean>;                  // {1}


  constructor( public authS: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn$ = this.authS.isLoggedIn;
  }

  cerrarSesion(){
    this.authS.logout();
    this.router.navigate(['/inicio']);
  }


}
