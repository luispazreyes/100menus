import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
import { CargaImagenesService } from 'src/app/services/carga-imagenes.service';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';


import Swal from 'sweetalert2';
import { AngularFireStorage, AngularFireStorageReference } from 'angularfire2/storage';
import { finalize } from 'rxjs/operators';
import { ValidadoresService } from 'src/app/services/validadores.service';
import { RestauranteModel } from 'src/app/models/restaurante.model';
import { RestauranteService } from 'src/app/services/restaurante.service';


@Component({
  selector: 'app-restaurante-perfil',
  templateUrl: './restaurante-perfil.component.html',
  styleUrls: ['./restaurante-perfil.component.scss']
})
export class RestaurantePerfilComponent implements OnInit {

  forma: FormGroup;
  cargando: boolean = false;
  restaurantesIds: string[] = [];
  menuExtension: string;
  menuPreview: any;
  logoPreview: any;
  showPreview = false;
  menuPaths: string[] =[];
  logoPath: string;
  logo: File = null;
  fileRef: AngularFireStorageReference[] = []
  menus: File[] = [];
  progreso: number = 0;
  downloadURL: Observable<string>;
  restaurante: RestauranteModel = new RestauranteModel();
  subscripcion: number = 50;
  menuNames: string[] = [];
  logoNames: string[] = [];

  restaurantes: RestauranteModel[] = [];

  product = {
    price: String(this.restaurantes.length * 50),
    description: 'Subscripción 100 Menús'
  };

  constructor(private storage: AngularFireStorage,
    private usuarioService: UsuarioService,
    private cargaService: CargaImagenesService,
    private fb: FormBuilder,
    private router: Router,
    private validadores: ValidadoresService,
    private restauranteService: RestauranteService) {
      this.crearFormulario();
    }

  irPago(){
  

    this.router.navigate(['/paypal'] , { queryParams: { restaurantesIds: this.restaurantesIds, subscripcion: this.subscripcion } });
  }  

  borrarRestaurante(event: any, id: string, index: number){
    event.preventDefault();
    Swal.fire({
      title: '¿Está seguro de borrar el restaurante?',
      text: "Los cargos cobrados no seran devueltos",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrarlo!'
    }).then((result) => {
      if (result.value) {

        this.restauranteService.borrarRestaurante(id).subscribe( res =>{
          Swal.fire(
            'Eliminado!',
            'El restaurante ha sido eliminado',
            'success'
          );
          this.restaurantes.splice(index, 1);

        });

      }
    })
  }


  ngOnInit() {
    

    this.cargando = true;
    this.restauranteService.getRestaurantesByUser()
      .subscribe( resp => {
        this.restaurantes = resp;
        // this.restaurantes.forEach((val,index) => {
        //   this.restaurantesIds.push(this.restaurantes[index].id);
        // });
        this.cargando = false;
      });
  }

  get nombreRestauranteNoValido() {
    return this.forma.get('nombreRestaurante').invalid && this.forma.get('nombreRestaurante').touched
  }

  get direccionNoValido() {
    return this.forma.get('direcciones').invalid && this.forma.get('direcciones').touched
  }

  get direcciones() {
    return this.forma.get('direcciones') as FormArray;
  }

  // set direcciones(fb: any){
  //   this.direcciones = fb;
  // }

  crearFormulario() {

    let formArray = new FormArray([
      new FormControl('', [Validators.required, Validators.minLength(3)])
    ]);

    this.forma = this.fb.group({
      nombreRestaurante : ['', [Validators.required, Validators.minLength(3)]],
      direcciones: this.fb.array([this.createDireccionFormGroup()],
      [Validators.required])
    });

  }

  createDireccionFormGroup() {
    return this.fb.group({
      direccion: ['', [Validators.required, Validators.min(3)]]
    });
  } 

  goPreview(codigo: string){
    this.router.navigate(['/restaurante-menu', codigo]);
  }

  onFileSelected(event: File[], tipo: string) {
    
    const n = Date.now();
    const file: File = event[0];
    const extension = event[0].name.substr(event[0].name.indexOf('.'));
    const blob = file.slice(0, file.size, file.type); 
    const newFile = new File([blob], `${n}${extension}`, {type: file.type});
    var mimeType = event[0].type;
    var reader = new FileReader();

    
    // const fileRef = this.storage.ref(filePath);
    
    switch (tipo) {
      case 'menu':
        
        if (mimeType.match(/image\/*/) == null) {
          this.menuExtension = 'pdf'
        }else{
          // this.imagePath = event;
          this.menuExtension = 'image'
          reader.readAsDataURL(event[0]); 
          reader.onload = (_event) => { 
            this.menuPreview = reader.result; 
          }
        }
        this.menus.push(newFile);
        this.menuPaths.push(`menus/${n}`);
        this.menuNames.push(`${n}`);
        break;
    
      default:
        if (mimeType.match(/image\/*/) == null) {
          return;
        }else{
         
          // this.imagePath = event;
          reader.readAsDataURL(event[0]); 
          reader.onload = (_event) => { 
            this.logoPreview = reader.result; 
          }
        }


        this.logo = newFile;
        this.logoPath = `logos/${n}`;
        this.logoNames.push(`${n}`);

        break;
    }
  }

  cambiarSubscripion(subscricion: string){
    
  }

  agregarDireccion() {
    let fg = this.createDireccionFormGroup();
    this.direcciones.push(  fg  );
  }

  borrarDireccion(i: number) {
    this.direcciones.removeAt(i);
  }

  crearRestauranteDirecciones(){

    this.restaurante.codigoRestaurante = this.forma.get('nombreRestaurante').value.replace(/\s+/g, '');
    this.restaurante.codigoRestaurante = this.restaurante.codigoRestaurante.toLowerCase();
    this.restaurante.nombreRestaurante = this.forma.get('nombreRestaurante').value.replace(/\b\w/g, function(l){ return l.toUpperCase() });    
    this.restaurante.usuario = localStorage.getItem('usuarioDocId');
    this.restaurante.menuTipo = this.menuExtension;

    let promise = new Promise(resolve => {
      let direcciones: string[] = this.forma.get('direcciones').value;
      

      direcciones.forEach((val: any, i) =>{
        let codigoTemporal = this.forma.get('nombreRestaurante').value.replace(/\s+/g, '');
        codigoTemporal = codigoTemporal.toLowerCase();
        codigoTemporal = codigoTemporal+(i+1);
        this.restaurante.direccion = val.direccion;
        this.restaurante.codigoRestaurante = codigoTemporal;
        this.restaurante.menuName = this.menuNames[0];
        this.restaurante.logoName = this.logoNames[0];
        
        let peticion: Observable<any>;
    
    
        peticion = this.restauranteService.guardarRestaurante( this.restaurante );
        
    
        peticion.subscribe( resp => {
          // this.restaurantes.push(this.restaurante);

          if( (direcciones.length-1) == i){
            this.cargando = true;
            this.restauranteService.getRestaurantesByUser()
              .subscribe( resp => {
                this.restaurantes = resp;
                // this.restaurantes.forEach((val,index) => {
                //   this.restaurantesIds.push(this.restaurantes[index].id);
                // });
                this.cargando = false;
                resolve(true);
              });
          }
        });
      });
  
    });

    return promise;

  }

  crearRestaurante(){
    if ( this.forma.invalid ) {
  
      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( control => control.markAsTouched() );
        } else {
          control.markAsTouched();
        }

      });
    }

    if(this.direcciones.length < 1){
      Swal.fire({
        icon: 'error',
        title: 'Debe ingresar al menos una dirección',
        text: 'Por ingrese la dirección de su restaurante',
      });
    }else{

      this.verificarNombreRestaurante().then(res => {
        if(!res){
          Swal.fire({
            icon: 'error',
            title: 'El nombre del restaurante ya ha sido tomado',
            text: 'Por favor escoja otro nombre',
          });
        }else{
          if(this.menus.length == 0 || this.logo == null){
            Swal.fire({
              icon: 'error',
              title: 'Debe seleccionar el menú y el logo del restaurante',
              // text: 'Usuario Registrado Exitosamente',
            });
          } else {
      
            Swal.fire({
              icon: 'info',
              title: 'Espere',
              text: 'Guardando información',
              allowOutsideClick: false
            });
            Swal.showLoading();
      
            this.subirAchivos('menu').then((url: string) => {
              this.restaurante.menu = url;
              this.subirAchivos('logo').then((url: string) => {
                this.restaurante.logo = url;
                this.crearRestauranteDirecciones().then(res => {
                  Swal.fire({
                    title: this.restaurante.nombreRestaurante,
                    text: 'Restaurante Creado Exitosamente',
                    icon: 'success'
                  });
            
                  this.forma.reset();
                  this.direcciones.controls = [];
                  this.menus = [];
                  this.logo = null;
                  this.menuPreview = null;
                });
                
              })
            });
          }
        }
      });
    }
  }

  verificarNombreRestaurante(){

    let promise = new Promise( resolve => {

      let direcciones: string[] = this.forma.get('direcciones').value;
      direcciones.forEach((val: any, i) =>{
        let codigoTemporal = this.forma.get('nombreRestaurante').value.replace(/\s+/g, '');
        codigoTemporal = codigoTemporal.toLowerCase();
        codigoTemporal = codigoTemporal+(i+1);
        
        this.restauranteService.getRestaurantesByCode(codigoTemporal)
          .subscribe( resp => {
            if(resp.length > 0){
              resolve(false);
            }else{
              resolve(true);
            }
          });
      });
      
    });

    return promise;

  }

  public subirAchivos(fileType: string){

    let promise = new Promise(resolve => {
      switch (fileType) {
        case 'menu':
          this.menus.forEach((value: File, index: number) => {
      
            const fileRef = this.storage.ref(this.menuPaths[index]);
            const task = this.storage.upload(this.menuPaths[index], value);
            task
              .snapshotChanges()
              .pipe(
                finalize(() => {
                  this.downloadURL = fileRef.getDownloadURL();
                  this.downloadURL.subscribe((url: any) => {
                    if (url) {
                      resolve(url);
                    }
                  });
                })
              )
              .subscribe(url => {
                if (url) {
                  this.progreso = ( url.bytesTransferred / url.totalBytes ) * 100
                }
              });
          });
          
          break;
      
        default:
          const fileRef = this.storage.ref(this.logoPath);
            const task = this.storage.upload(this.logoPath, this.logo);
            task
              .snapshotChanges()
              .pipe(
                finalize(() => {
                  this.downloadURL = fileRef.getDownloadURL();
                  this.downloadURL.subscribe((url: any) => {
                    if (url) {
                      resolve(url);
                    }
                  });
                })
              )
              .subscribe(url => {
                if (url) {
                  this.progreso = ( url.bytesTransferred / url.totalBytes ) * 100
                }
              });
          break;
      }
    });
    
    return promise;

  }

  addRestauranteId(event: any, restauranteId: string) {
    if (event.target.checked) {
     this.restaurantesIds.push(event.target.value);
    } else {
      this.restaurantesIds.splice(this.restaurantesIds.indexOf(restauranteId),1);
    }
}

}
