import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { UsuarioService } from '../../../services/usuario.service';
import { RestauranteService } from '../../../services/restaurante.service';
import { RestauranteModel } from 'src/app/models/restaurante.model';


declare var paypal;

@Component({
  selector: 'app-restaurante-menu',
  templateUrl: './restaurante-menu.component.html',
  styleUrls: ['./restaurante-menu.component.css']
})
export class RestauranteMenuComponent implements OnInit {
  @ViewChild('paypal', { static: true}) paypalElement: ElementRef;

  product = {
    price: 0,
    description: 'used couch, decent condition'
  };

  
  
  constructor(private route: ActivatedRoute,
    private restauranteService: RestauranteService, 
    private router: Router) { }

  public menus = [];
  public show = false;
  public showPagar = true;
  public logo;
  public type = '';
  public usuario;
  public restaurante: RestauranteModel;
  subscripcion: number = 50;


  ngOnInit(): void {
    this.restauranteService.getRestaurantesByCode(this.route.snapshot.paramMap.get("codigo"))
      .subscribe( resp => {
        this.restaurante = resp[0];
        if( this.restaurante.activo){
          this.show = true;
          this.showPagar=false;
        }else{
          if(localStorage.getItem('usuarioDocId') != undefined){
            this.show = true;
          }
        }
      });
      
  }

  irPago(){
    this.router.navigate(['/paypal'] , { queryParams: { restaurantesIds: [this.restaurante.id], subscripcion: this.subscripcion } });
  }  
  

  goToUrl(): void {
    location.href = this.restaurante.menu;
  }
}
