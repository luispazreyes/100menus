import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestauranteService } from 'src/app/services/restaurante.service';
import { RestauranteModel } from 'src/app/models/restaurante.model';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AngularFireStorageReference } from 'angularfire2/storage';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-restaurante-edit',
  templateUrl: './restaurante-edit.component.html',
  styleUrls: ['./restaurante-edit.component.css']
})
export class RestauranteEditComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private restauranteService: RestauranteService,
    private fb: FormBuilder,) { }
    
    forma: FormGroup;

    cargando: boolean = false;
    restaurantesIds: string[] = [];
    menuExtension: string;
    menuPreview: any;
    logoPreview: any;
    showPreview = false;
    menuPaths: string[] =[];
    logoPath: string;
    logo: File = null;
    fileRef: AngularFireStorageReference[] = []
    menus: File[] = [];
    progreso: number = 0;
    downloadURL: Observable<string>;
    public restaurante: RestauranteModel
    show: boolean = false;


  ngOnInit(): void {
    this.restauranteService.getRestaurantesByCode(this.route.snapshot.paramMap.get("codigo"))
      .subscribe( resp => {
        this.restaurante = resp[0];
          if(localStorage.getItem('usuarioDocId') === this.restaurante.usuario){
            this.show = true;
            this.cargarDataAlFormulario();
          }
      });
      
      this.crearFormulario();
    }

    get nombreRestauranteNoValido() {
      return this.forma.get('nombreRestaurante').invalid && this.forma.get('nombreRestaurante').touched
    }
  
    get direccionNoValido() {
      return this.forma.get('direccion').invalid && this.forma.get('direccion').touched
    }
  
    // get direcciones() {
    //   return this.forma.get('direcciones') as FormArray;
    // }

  actualizarRestaurante(){

  }

  
  crearFormulario() {

    this.forma = this.fb.group({
      nombreRestaurante : ['', [Validators.required, Validators.minLength(3)]],
      direccion: ['', [Validators.required, Validators.minLength(3)]],
    });

  }

  cargarDataAlFormulario() {

    this.forma.reset({
      nombreRestaurante: this.restaurante.nombreRestaurante,
      direccion: this.restaurante.direccion
    });


  }

  onFileSelected(event: File[], tipo: string) {
    
    const n = Date.now();
    const file: File = event[0];
    const extension = event[0].name.substr(event[0].name.indexOf('.'));
    const blob = file.slice(0, file.size, file.type); 
    const newFile = new File([blob], `${n}${extension}`, {type: file.type});
    var mimeType = event[0].type;
    var reader = new FileReader();

    
    // const fileRef = this.storage.ref(filePath);
    
    switch (tipo) {
      case 'menu':
        
        if (mimeType.match(/image\/*/) == null) {
          this.menuExtension = 'pdf'
        }else{
          // this.imagePath = event;
          reader.readAsDataURL(event[0]); 
          reader.onload = (_event) => { 
            this.menuPreview = reader.result; 
          }
        }
        this.menus.push(newFile);
        this.menuPaths.push(`menus/${n}`);
        break;
    
      default:
        if (mimeType.match(/image\/*/) == null) {
          return;
        }else{
         
          // this.imagePath = event;
          reader.readAsDataURL(event[0]); 
          reader.onload = (_event) => { 
            this.logoPreview = reader.result; 
          }
        }


        this.logo = newFile;
        this.logoPath = `logos/${n}`;
        break;
    }
  }

}
