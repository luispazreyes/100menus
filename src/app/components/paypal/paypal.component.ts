import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { RestauranteService } from '../../services/restaurante.service';
// import { resolve } from 'dns';

declare var paypal;
import Swal from 'sweetalert2';


@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {
  @ViewChild('paypal', { static: true}) paypalElement: ElementRef;

product = {
  price: 0,
  description: 'used couch, decent condition'
};

paidFor = false;

  restaurantesIds: string[] = [];
  subscripcion: number = 0;


  // We need use the ActivatedRoute object to get access 
  // to information about the current route
  constructor(private route: ActivatedRoute, private restauranteS: RestauranteService, private router: Router){

    this.route.queryParams.subscribe(params => {
      this.restaurantesIds = params['restaurantesIds']; 
      this.subscripcion = params['subscripcion']
      this.product.price = this.restaurantesIds.length * this.subscripcion;    
    });
  }

  ngOnInit(): void {
    paypal
    .Buttons({
      createOrder: (data, actions) => {
        return actions.order.create({
          purchase_units: [
            {
              description: this.product.description,
              amount: {
                currency_code: 'USD',
                value: this.product.price
              }
            }
          ]
        });
      },
      onApprove: async (data, actions) => {
        const order = await actions.order.capture();
        this.paidFor = true;
        Swal.fire({
          icon: 'info',
          title: 'Espere',
          text: 'Generando Código QR',
          allowOutsideClick: false
        });
        Swal.showLoading();
        this.restauranteS.activarRestaurantes(this.restaurantesIds, this.subscripcion).then(res =>{
          if(res){
            Swal.fire({
              title: 'Subscripción Activada',
              text: 'Código QR Generado Existosamente',
              icon: 'success',
              onClose: () => {
                this.router.navigate(['/restaurante-perfil']);
              }
            });
          }
        });
      },
      onError: err => {
        console.log(err);
      },
      onAuthorize: function (data, actions) {
        // Get the payment details
        return actions.payment.get()
          .then(function (paymentDetails) {
            // Show a confirmation using the details from paymentDetails
            // Then listen for a click on your confirm button
            document.querySelector('#confirm-button')
              .addEventListener('click', function () {
                // Execute the payment
                return actions.payment.execute()
                  .then(function () {
                    // Show a success page to the buyer
                  });
              });
          });
      }
    })
    .render(this.paypalElement.nativeElement);
  }

}
