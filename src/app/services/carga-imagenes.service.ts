import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { FileItem } from '../models/file-item';
import { AngularFireStorage } from 'angularfire2/storage';


@Injectable()
export class CargaImagenesService {

  private CARPETA_IMAGENES = 'menus';

  constructor( private db: AngularFirestore,
    private storage: AngularFireStorage ) { }


  cargarImagenesFirebase( imagenes: FileItem[], docId: any ) {

    let promise = new Promise((resolve) => {

      const storageRef = firebase.storage().ref();
  
      for ( const item of imagenes ) {
  
        item.estaSubiendo = true;
        if ( item.progreso >= 100 ) {
          continue;
        }
  
        // this.validarSiExiste(storageRef, item.nombreArchivo);
  
        const uploadTask: firebase.storage.UploadTask =
                    storageRef.child(`${ this.CARPETA_IMAGENES }/${ item.nombreArchivo }`)
                              .put( item.archivo );
  
        uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
                ( snapshot: firebase.storage.UploadTaskSnapshot ) =>
                            item.progreso = ( snapshot.bytesTransferred / snapshot.totalBytes ) * 100,
                ( error ) => console.error('Error al subir', error ),
                () => {
  
                  console.log('Imagen cargada correctamente');
                  uploadTask.snapshot.ref.getDownloadURL().then(url => {
                    item.url = url;
                    item.estaSubiendo = false;
                    this.guardarImagen({
                      nombre: item.nombreArchivo,
                      url: item.url,
                      usuarioDocId: docId
                    }, 'menu').then(res => {
                      if(res){
                        resolve(true);
                      }else{
                        resolve(false);
                      }
                    });
                  });
                });
      }
    });

    return promise;

  }

  // private validarSiExiste(storageRef: firebase.storage.Reference, archivo: string) {
  //   storageRef.child(`/${ this.CARPETA_IMAGENES }/${archivo}`).getDownloadURL().then(res => {
  //     console.log(res);
  //   });
  // }


  guardarImagen( imagen: { nombre: string, url: string, usuarioDocId: string }, collectionName: string ) {

    let promise = new Promise((resolve) => {

      this.db.collection(`/${ collectionName }`)
              .add( imagen ).then( res => {
                resolve(true);
              }).catch((error) => {
                console.log(error);
                resolve(false);
              });
    });

    return promise;

  }

   //Tarea para subir archivo
   public tareaCloudStorage(nombreArchivo: string, datos: any) {
    return this.storage.upload(nombreArchivo, datos);
  }

  //Referencia del archivo
  public referenciaCloudStorage(nombreArchivo: string) {
    return this.storage.ref(nombreArchivo);
  }

}
