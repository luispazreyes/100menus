import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import {UsuarioModel} from '../models/usuario.model'
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { map, delay } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UsuarioService {

private CARPETA_USUARIOS = 'usuarios';

private url = 'https://menus-284804.firebaseio.com';

  constructor(private http: HttpClient,
			  private afAuth: AngularFireAuth,
			  private db: AngularFirestore ) {}

  registrarUser(email: string, password: string) {
	  let promise = new Promise((resolve) => {
		  this.afAuth.auth.createUserWithEmailAndPassword(email, password).then((user) => {
			resolve(user.user.uid);
		   }).catch(response => {
		  	resolve(false);
		  });
	  });

	  return promise;
  }

  getUsuarioByCorreo(correo: string) {

    
    return this.http.get(`${ this.url }/usuarios.json?orderBy="correo"&equalTo="${correo}"`)
            .pipe(
              map( this.crearArreglo ),
              delay(0)
            );
  }

  private crearArreglo( UsuariosObj: object ) {

    const usuarios: UsuarioModel[] = [];

    Object.keys( UsuariosObj ).forEach( key => {

      const usuario: UsuarioModel = UsuariosObj[key];
      usuario.id = key;

      usuarios.push( usuario );
    });


    return usuarios;

  }

  guardarUsuario( usuario: UsuarioModel ) {

	// let promise = new Promise((resolve) => {

	// 	this.db.collection(`/${ this.CARPETA_USUARIOS }`)
	// 		.add( {...usuario} ).then( docRef => {
	// 			localStorage.setItem('usuarioDocId', docRef.id);
	// 			resolve(docRef.id);
	// 		})
	// 		.catch(function(error) {
	// 			resolve(false);
	// 			console.error("Error adding document: ", error);
	// 	});
	// });

	// return promise;

	return this.http.post(`${ this.url }/usuarios.json`, usuario)
					.pipe(
					  map( (resp: any) => {
						localStorage.setItem('usuarioDocId', resp.name);
						usuario.id = resp.name;
						return usuario;
					  })
					);
  }

  getMenu(){
	  const docId = localStorage.getItem('usuarioDocId');
	  return this.db.collection('menus', ref => ref.where('usuarioDocId', '==', docId)).snapshotChanges();
  }

  getLogo(){
	const docId = localStorage.getItem('usuarioDocId');
	return this.db.collection('logos', ref => ref.where('usuarioDocId', '==', docId)).snapshotChanges();
  }

  getUsuario(){
	const docId = localStorage.getItem('usuarioDocId');
	return this.db.collection('usuarios').doc(docId).snapshotChanges();
  }

  getMenuByCode(codigo: string){
	let promise = new Promise(resolve =>{

		this.db.collection('usuarios', ref => ref.where('codigo_restaurante', '==', codigo)).snapshotChanges().subscribe((usuarioSnapshot) => {
			usuarioSnapshot.forEach((usuarioData: any) => {				
				localStorage.setItem('usuarioDocId', usuarioData.payload.doc.id);
				resolve(true);
			})
		  });
	});

	return promise;
	  
  }
   
}