import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';

import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty';
  private apikey = 'AIzaSyAFPWOdREa_3m5CYl0edUzSDXAFXUyTHco';
  private loggedIn = new BehaviorSubject<boolean>(false); // {1}

  userToken: string;

  // Crear nuevo usuario
  // https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=[API_KEY]


  // Login
  // https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=[API_KEY]


  constructor( private http: HttpClient, private afAuth: AngularFireAuth ) {
    this.leerToken();
  }


  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }


  logout() {
    this.loggedIn.next(false);
    localStorage.removeItem('token');
  }

  login( usuario: UsuarioModel ) {

    // const authData = {
    //   ...usuario,
    //   returnSecureToken: true
    // };

    // return this.http.post(
    //   `${ this.url }/verifyPassword?key=${ this.apikey }`,
    //   authData
    // ).pipe(
    //   map( resp => {
    //     this.guardarToken( resp['idToken'] );
    //     return resp;
    //   })
    // );

    let promise = new Promise((resolve, reject) =>{
      this.afAuth.auth.signInWithEmailAndPassword(usuario.correo, usuario.password)
      .then(res => {
        this.guardarToken( res.user.refreshToken );
        this.loggedIn.next(true);
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
    });

    return  promise;

  }

  nuevoUsuario( usuario: UsuarioModel ) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post(
      `${ this.url }/signupNewUser?key=${ this.apikey }`,
      authData
    ).pipe(
      map( resp => {
        this.guardarToken( resp['idToken'] );
        return resp;
      })
    );

  }


  private guardarToken( idToken: string ) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds( 3600 );

    localStorage.setItem('expira', hoy.getTime().toString() );


  }

  leerToken() {

    if ( localStorage.getItem('token') ) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;

  }


  estaAutenticado(): boolean {

    if ( this.userToken.length < 2 ) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if ( expiraDate > new Date() ) {
      return true;
    } else {
      return false;
    }


  }


}
