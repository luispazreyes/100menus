import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestauranteModel } from '../models/restaurante.model';
import { map, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {formatDate} from '@angular/common';
import { AngularFireStorage } from 'angularfire2/storage';

import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class RestauranteService {

  private url = 'https://menus-284804.firebaseio.com';

  constructor(private http: HttpClient, private storage: AngularFireStorage) { }

  guardarRestaurante( restaurante: RestauranteModel ) {

    return this.http.post(`${ this.url }/restaurantes.json`, restaurante)
            .pipe(
              map( (resp: any) => {
              restaurante.id = resp.name;
              return restaurante;
              })
            );
  }

  getRestaurantesByUser() {
    const docId = localStorage.getItem('usuarioDocId');
    
    return this.http.get(`${ this.url }/restaurantes.json?orderBy="usuario"&equalTo="${docId}"`)
            .pipe(
              map( this.crearArreglo ),
              delay(0)
            );
  }

  borrarRestaurante( id: string ) {
      return this.http.delete(`${ this.url }/restaurantes/${ id }.json`);
  }

  borrarFile(url: string, tipo: string){
    const storageRef = firebase.storage().ref()
    return storageRef.child(`${tipo}/${url}`).delete();
  }

  getRestaurantesByCode(codigo: string) {

    
    return this.http.get(`${ this.url }/restaurantes.json?orderBy="codigoRestaurante"&equalTo="${codigo}"`)
            .pipe(
              map( this.crearArreglo ),
              delay(0)
            );
  }

  addMonths(date, months) {
    var d = date.getDate();
    date.setMonth(date.getMonth() + +months);
    if (date.getDate() != d) {
      date.setDate(0);
    }
    return date;
}

  activarRestaurantes(ids: string[], subscripcion: number){
    var oneYearFromNow = new Date();
    if(subscripcion == 50){
      oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() + 1);
    }else{
      
      oneYearFromNow = this.addMonths(oneYearFromNow,1)
    }

    let promise = new Promise(resolve =>{

      ids.forEach((val, index) => {
        this.getRestaurante(val).subscribe( (res: RestauranteModel) => {
          let restaurante: RestauranteModel = res;
          restaurante.activo = true;
          restaurante.id = val;
          
          // restaurante.fechaVencimiento = String(oneYearFromNow);
          restaurante.fechaActivacion = formatDate(new Date(), 'yyyy/MM/dd', 'en');
          restaurante.fechaVencimiento = formatDate(oneYearFromNow, 'yyyy/MM/dd', 'en');
          let peticion: Observable<any>;
          peticion = this.actualizarRestaurante(restaurante);
          peticion.subscribe(res => {
            if(index == (ids.length -1)){

              resolve(true);
            }
          })
        })
      });
    })

    return promise;
  }

  actualizarRestaurante( restaurante: RestauranteModel ) {

    const restauranteTemp = {
      ...restaurante
    };

    delete restauranteTemp.id;

    return this.http.put(`${ this.url }/restaurantes/${ restaurante.id }.json`, restauranteTemp);

  }

  getRestaurante( id: string ) {

    return this.http.get(`${ this.url }/restaurantes/${ id }.json`);

  }

  private crearArreglo( restaurantesObj: object ) {

    const restaurantes: RestauranteModel[] = [];

    Object.keys( restaurantesObj ).forEach( key => {

      const heroe: RestauranteModel = restaurantesObj[key];
      heroe.id = key;

      restaurantes.push( heroe );
    });

    return restaurantes;

  }
}
