import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateComponent } from './pages/template/template.component';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { CargaImagenesService } from './services/carga-imagenes.service';

import { NgDropFilesDirective } from './directives/ng-drop-files.directive';
import { UsuarioService } from './services/usuario.service';
import { RestaurantePerfilComponent } from './components/restaurante/restaurante-perfil/restaurante-perfil.component';

import { QriousModule } from 'angular-qrious';
import { RestauranteMenuComponent } from './components/restaurante/restaurante-menu/restaurante-menu.component';
import { RestauranteService } from './services/restaurante.service';
import { PaypalComponent } from './components/paypal/paypal.component';
import { LoginComponent } from './components/login/login.component';
import { QrComponent } from './components/qr/qr.component';
import {NgxPrintModule} from 'ngx-print';
import { RestauranteEditComponent } from './components/restaurante/restaurante-edit/restaurante-edit.component';




@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent,
    ReactiveComponent,
    HomeComponent,
    NavbarComponent,
    NgDropFilesDirective,
    RestaurantePerfilComponent,
    RestauranteMenuComponent,
    PaypalComponent,
    LoginComponent,
    QrComponent,
    RestauranteEditComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    QriousModule,
    NgxPrintModule
  ],
  providers: [CargaImagenesService, UsuarioService, RestauranteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
