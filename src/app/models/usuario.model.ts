export class UsuarioModel {
    id: string;
    uid: string;
    nombreCompleto: string;
    telefono: string;
    correo: string;
    codigo_area: string;
    pais: string;
    password: string;
}