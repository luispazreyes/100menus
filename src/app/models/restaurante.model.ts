export class RestauranteModel{
    id: string;
    usuario: string;
    nombreRestaurante: string;
    codigoRestaurante: string;
    menu: string;
    logo: string;
    direccion: string;
    activo: boolean = false;
    fechaActivacion: string;
    fechaVencimiento: string;
    menuName: string;
    logoName: string;
    menuTipo: string;
}