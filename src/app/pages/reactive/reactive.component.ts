import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ValidadoresService } from '../../services/validadores.service';
import { UsuarioService } from '../../services/usuario.service';
import { Observable } from 'rxjs';
import { FileItem } from '../../models/file-item';
import { CargaImagenesService } from '../../services/carga-imagenes.service';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { countries } from '../../data/countries';
import { calling_codes } from '../../data/countries copy';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']})
export class ReactiveComponent implements OnInit {

  paises = countries;
  forma: FormGroup;
  selectedFiles: FileList;
  archivos: FileItem[] = [];
  estaSobreElemento = false;
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  usuario: UsuarioModel = new UsuarioModel();

  public archivoForm = new FormGroup({
    archivo: new FormControl(null, Validators.required),
  });
  
  public mensajeArchivo = 'No hay un archivo seleccionado';
  public datosFormulario = new FormData();
  public nombreArchivo = '';
  public URLPublica = '';
  public porcentaje = 0;
  public finalizado = false;


  constructor( private fb: FormBuilder,
               private validadores: ValidadoresService,
               private usuarioS: UsuarioService,
               private router: Router, private auth: AuthService ) {

    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get pasatiempos() {
    return this.forma.get('pasatiempos') as FormArray;
  }

  get nombreNoValido() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched
  }

  

  get paisNoValido() {
    return this.forma.get('pais').invalid && this.forma.get('pais').touched
  }

  get correoNoValido() {
    return this.forma.get('correo').invalid && this.forma.get('correo').touched
  }


  get telefonoNoValido() {
    return this.forma.get('telefono').invalid && this.forma.get('telefono').touched
  }

  get codigoNoValido() {
    return this.forma.get('codigo_area').invalid && this.forma.get('codigo_area').touched
  }

  get direccionNoValido() {
    return this.forma.get('direccion.direccion').invalid && this.forma.get('direccion.direccion').touched
  }

  get ciudadNoValido() {
    return this.forma.get('direccion.ciudad').invalid && this.forma.get('direccion.ciudad').touched
  }

  get pass1NoValido() {
    return this.forma.get('pass1').invalid && this.forma.get('pass1').touched;
  }

  get pass2NoValido() {
    const pass1 = this.forma.get('pass1').value;
    const pass2 = this.forma.get('pass2').value;

    return ( pass1 === pass2 ) ? false : true;
  }


  crearFormulario() {

    this.forma = this.fb.group({
      telefono : ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      codigo_area : ['', [Validators.required]],
      nombre  :  ['', [ Validators.required, Validators.minLength(3) ]  ],
      correo  :  ['', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] ],
      pais : ['', [Validators.required]],
      pass1   :  ['', [Validators.required, Validators.minLength(6)] ],
      pass2   :  ['', Validators.required ],
    },{
      validators: this.validadores.passwordsIguales('pass1','pass2')
    });

  }

  setCodigoArea(index: any){
    this.forma.controls['codigo_area'].setValue(calling_codes[index]);
    this.forma.controls['codigo_area'].patchValue(calling_codes[index]);    
  }

  guardar() {
 
      if ( this.forma.invalid ) {
  
        return Object.values( this.forma.controls ).forEach( control => {
          if ( control instanceof FormGroup ) {
            Object.values( control.controls ).forEach( control => control.markAsTouched() );
          } else {
            control.markAsTouched();
          }
  
        });
      }
      this.usuarioS.registrarUser(this.forma.get('correo').value, this.forma.get('pass1').value).then((res: string) => {
        if(res){
      
          this.usuario.nombreCompleto = this.forma.get('nombre').value;
          this.usuario.correo = this.forma.get('correo').value;
          this.usuario.telefono = this.forma.get('telefono').value;
          this.usuario.codigo_area = this.forma.get('codigo_area').value;
          this.usuario.pais = this.forma.get('pais').value;
          this.usuario.uid = res;
  

          Swal.fire({
            icon: 'info',
            title: 'Espere',
            text: 'Creando Usuario',
            allowOutsideClick: false
          });
          Swal.showLoading();
      
      
          let peticion: Observable<any>;
      
     
          peticion = this.usuarioS.guardarUsuario( this.usuario );
          
      
          peticion.subscribe( resp => {
      
            Swal.fire({
              title: this.usuario.correo,
              text: 'Usuario Creado Exitosamente',
              icon: 'success'
            });

            this.usuario.password =  this.forma.get('pass1').value;

            this.auth.login( this.usuario ).then( res => {
              
      
              
              localStorage.setItem('email', this.usuario.correo);
              
              this.router.navigateByUrl('/restaurante-perfil');
          }).catch(err => {
            console.log(err.message);
              Swal.fire({
                icon: 'error',
                title: 'Error al autenticar',
                text: err.message
              });
          });

            // this.router.navigate(['/restaurante-perfil']);

          });
      
  
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Este correo ya esta en uso',
            // text: 'Usuario Registrado Exitosamente',
          });
        }
      });
    
  }

}
