// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase : {
    apiKey: "AIzaSyAFPWOdREa_3m5CYl0edUzSDXAFXUyTHco",
    authDomain: "menus-284804.firebaseapp.com",
    databaseURL: "https://menus-284804.firebaseio.com",
    projectId: "menus-284804",
    storageBucket: "menus-284804.appspot.com",
    messagingSenderId: "120907083447",
    appId: "1:120907083447:web:28e38dc380824577b82732",
    measurementId: "G-4WNFF8XVJ9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
